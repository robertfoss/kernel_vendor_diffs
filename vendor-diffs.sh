#!/usr/bin/env bash
#
# This script is intended to be run from an already existing linux git repo.
#

# Exit on error. Append "|| true" if you expect an error.
set -e
# Turn on traces, useful while debugging but commented out by default
#set -x

# Get kernel version, exports $VERSION.$PATCHLEVEL.$SUBLEVEL
function get_version()
{
    git checkout $1 > /dev/null 2>&1
    export $(grep -o '^[^#]*' Makefile | head -3 | sed 's/ //g')
}

function get_statistics()
{
    # Number of commits ignoring commits with more than one parent
    echo -n "$(git log --no-merges --pretty=oneline v$VERSION.$PATCHLEVEL.$SUBLEVEL..$1 | wc -l)"
    # Files changed, insertions and deletions
    echo "$(git diff v$VERSION.$PATCHLEVEL.$SUBLEVEL $1 --shortstat | awk '{ print "," $1 "," $4 "," $6 }')"
}

# Stable
git remote add stable git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git > /dev/null 2>&1 || true
git fetch stable

# Freescale
FSLC_KERNEL_VERSIONS="4.9 4.10 4.11 4.12 4.13 4.14"
git remote add freescale git://github.com/Freescale/linux-fslc.git > /dev/null 2>&1 || true
git fetch freescale || true

# Texas Instruments
TILSK_KERNEL_VERSIONS="4.4 4.9 4.14"
git remote add tilsk git://git.ti.com/ti-linux-kernel/ti-linux-kernel.git > /dev/null 2>&1 ||true
git fetch tilsk || true

echo "vendor,vendor branch,stable version,number of commits,files changed,insertions,deletions"

for ver in $FSLC_KERNEL_VERSIONS; do
    get_version "freescale/$ver.x+fslc"
    echo -n "NXP,$ver.x+fslc,$VERSION.$PATCHLEVEL.$SUBLEVEL,"
    get_statistics "freescale/$ver.x+fslc"
done

for ver in $TILSK_KERNEL_VERSIONS; do
    get_version "tilsk/ti-lsk-linux-$ver.y"
    echo -n "Texas Instruments,ti-lsk-linux-$ver.y,$VERSION.$PATCHLEVEL.$SUBLEVEL,"
    get_statistics "tilsk/ti-lsk-linux-$ver.y"
done

