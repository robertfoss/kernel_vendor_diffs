#!/bin/bash

# This script is intended to be run from an already existing linux git repo.

# Remove crud from version name
function clean_version()
{                    
    echo $(echo "$1" | cut -f1 -d"-" )
}


# Upstream
git remote add git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git > /dev/null 2>&1
git fetch upstream

# Qualcomm
QCOM_KERNEL_VERSIONS="3.10 3.14 3.18 4.4 4.9"
for ver in $QCOM_KERNEL_VERSIONS; do
    git remote add qcom_msm-$ver https://source.codeaurora.org/quic/la/kernel/msm-$ver > /dev/null 2>&1
    git fetch qcom_msm-$ver
done

# HiSilicon
git remote add hisilicon https://github.com/hisilicon/linux-hisi > /dev/null 2>&1
git fetch hisilicon

# Samsung
SAMSUNG_KERNEL_VERSIONS="4.5 4.6 4.8 4.9 4.10-2 4.11 4.13 4.14 4.15"
git remote add samsung https://kernel.googlesource.com/pub/scm/linux/kernel/git/krzk/linux > /dev/null 2>&1
git fetch samsung

echo "--- Qualcomm ---"
for ver in $QCOM_KERNEL_VERSIONS; do
    echo "v$ver $(git diff qcom_msm-$ver/msm-$ver v$ver --shortstat | awk '{ print $4 + $6}')"
done

echo "--- HiSilicon ---"
#echo "v3.18 hix5hd2-dt-for-3.18: $(git diff hix5hd2-dt-for-3.18 v3.18 --shortstat | awk '{ print $4 + $6}')"
#echo "v4.4  hisi-soc-dt-for-4.4: $(git diff hisi-soc-dt-for-4.4 v4.4 --shortstat | awk '{ print $4 + $6}')"
echo "v4.9: $(git diff hisi-defconfig-for-4.9 v4.9 --shortstat | awk '{ print $4 + $6}')"
echo "v4.10 $(git diff hisi-arm64-dt-4.10 v4.10 --shortstat | awk '{ print $4 + $6}')"
echo "v4.11: $(git diff hisi-arm64-dt-for-4.11 v4.11 --shortstat | awk '{ print $4 + $6}')"
echo "v4.12: $(git diff hisi-arm64-dt-for-4.12 v4.12 --shortstat | awk '{ print $4 + $6}')"
echo "v4.13: $(git diff hisi-arm64-dt-for-4.13-v2 v4.13 --shortstat | awk '{ print $4 + $6}')"
echo "v4.14: $(git diff hisi-arm64-dt-for-4.14-v2 v4.14 --shortstat | awk '{ print $4 + $6}')"
echo "v4.15: $(git diff hisi-arm64-dt-for-4.15 v4.15 --shortstat | awk '{ print $4 + $6}')"

echo "--- Samsung ---"
for ver in $SAMSUNG_KERNEL_VERSIONS; do
    version=$(clean_version $ver)
    echo "v$version $(git diff samsung-soc-$ver v$version --shortstat | awk '{ print $4 + $6}')"
done
